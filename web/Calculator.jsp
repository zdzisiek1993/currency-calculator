<%@ page import="com.project.CurrencyCalculator" %><%--
  Created by IntelliJ IDEA.
  User: zdzislaw.oparowski
  Date: 2020-01-17
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="calculatorStyles.css">
    <title>Currency Calc</title>
</head>
<body>

<h1>Currency Calculator</h1>

<form method="get" action="Calculator.jsp">
    <b>Amount</b>
    <br>
    <input name="amount" type="number">
    <br>

    <b>Currency of provided amount</b>
    <br>
    <select name="currency1">
        <option value="pln">PLN</option>
        <option value="eur">EUR</option>
        <option value="usd">USD</option>
        <option value="chf">CHF</option>
    </select>
    <br>

    <b>Currency of exchange</b>
    <br>
    <select name="currency2">
        <option value="eur">EUR</option>
        <option value="usd">USD</option>
        <option value="chf">CHF</option>
        <option value="pln">PLN</option>
    </select>
    <br>

    <input type="submit" class="submitButton" value="Convert!">
</form>

<%
    String amount = request.getParameter("amount");
    String currency1 = request.getParameter("currency1");
    String currency2 = request.getParameter("currency2");

%>
<form class="result">
    <%
        if(amount != null && currency1 != null && currency2 != null){
            double amountAsDouble = Double.parseDouble(amount);
            double convertedAmount = CurrencyCalculator.calculate(amountAsDouble, currency1, currency2);
    %>
    <b><%=amount%> <%=currency1.toUpperCase()%></b> = <b><%=String.format("%.2f", convertedAmount)%> <%=currency2.toUpperCase()%></b>

    <% } %>
</form>

</body>
</html>
