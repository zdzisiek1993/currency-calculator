package com.project;

import java.util.HashMap;
import java.util.Map;

public class CurrencyCalculator {
    private static Map<String, Double> rates = new HashMap<>();

    static {
        rates.put("usd", 3.8);
        rates.put("pln", 1.0);
        rates.put("chf", 3.9);
        rates.put("eur", 4.3);
    }

    public static double calculate(double amount, String currency1, String currency2){
        currency1 = currency1.toLowerCase();
        currency2 = currency2.toLowerCase();

        double rateOfCurrency1 = rates.get(currency1);
        double rateOfCurrency2 = rates.get(currency2);

        return (amount * rateOfCurrency1) / rateOfCurrency2;
    }

    public static void main(String[] args) {
        double result = calculate(500,"eur", "usd");
        System.out.println(result);
    }

}
